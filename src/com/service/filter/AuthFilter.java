package com.service.filter;

/*************************************************************************
*  Cognizant Technology Pvt Ltd CONFIDENTIAL
*
*  All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
* the property of Cognizant Technology and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Cognizant Technology
 * and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Cognizant Technology.
*
Modification History
Date		Version		Author				Description
__________	___________	_______________		__________________________
15-05-2014	1.0			Vaibhav Dusaj	    Initial Version
**************************************************************************/

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthFilter implements Filter
{

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response,
						 final FilterChain filterChain) throws IOException, ServletException
	{
		if (response instanceof HttpServletResponse)
		{
			final HttpServletRequest     alterRequest = (HttpServletRequest)request;

			
			final Enumeration<String>    headerNames = alterRequest.getHeaderNames();

			while (headerNames.hasMoreElements())
			{
				final String    headerName = headerNames.nextElement();
				alterRequest.getHeader(headerName);
			}
		}
		filterChain.doFilter(request, response);
	}

	@Override
	public void destroy()
	{
		/**
		 * destroy method
		 */
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException
	{
		/**
		 * init method
		 */
	}
}
