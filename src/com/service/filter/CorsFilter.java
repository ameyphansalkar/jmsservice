package com.service.filter;

/*************************************************************************
*  Cognizant Technology Pvt Ltd CONFIDENTIAL
*
*  All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
* the property of Cognizant Technology and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Cognizant Technology
 * and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Cognizant Technology.
*
Modification History
Date		Version		Author				Description
__________	___________	_______________		__________________________
15-05-2014	1.0			Vaibhav Dusaj	    Initial Version
**************************************************************************/

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CorsFilter implements Filter
{


	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response,
						 final FilterChain filterChain)
						 throws IOException, ServletException
	{
		if (response instanceof HttpServletResponse)
		{
			final HttpServletResponse    alteredResponse = (HttpServletResponse) response;
			// I need to find a way to make sure this only gets called on
			// 200-300 http responses

			addHeadersFor200Response(alteredResponse);

			final HttpServletRequest     alterRequest = (HttpServletRequest)request;

			
			Enumeration<String> headerNames = alterRequest.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				headerNames.nextElement();
			}
		}

		filterChain.doFilter(request, response);
	}

	private void addHeadersFor200Response(HttpServletResponse response)
	{

		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
		response.addHeader("Access-Control-Allow-Headers",
				"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, AUTHORIZATION");
		response.addHeader("Access-Control-Max-Age", "1728000");
	}

	@Override
	public void destroy()
	{
		/*
		 * destroy method
		 */
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		/*
		 * init method
		 */
	}
}