package com.jmsservice.gateway;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

@Path("/services")
public class JMSGateWay {

	@POST
	@Path("/jmssender")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String jmsSender(String request) {
		
		JSONObject requestJson;
		try {
			requestJson = new JSONObject(request);
			String message = requestJson.getString("message");
			sendMessage(message);
			System.out.println(message);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "Message Sent";
	}	
	
	private void sendMessage(String text) {
        try {
            Context initCtx = new InitialContext();
            Context environmentContext = (Context)initCtx.lookup("java:/comp/env");
            ConnectionFactory connectionFactory = (ConnectionFactory) environmentContext.lookup("jms/ConnectionFactory");
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer((Destination)initCtx.lookup("java:comp/env/jms/queue/MyQueue"));

            TextMessage testMessage = session.createTextMessage();
            testMessage.setText(text);
            testMessage.setStringProperty("aKey", "someRandomTestValue");
            producer.send(testMessage);
            System.out.println("Successfully sent message.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	
	
	@GET
	@Path("/jmsreceiver")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public HashMap<String,String> jmsReceiver() {
		
		//String text = receiveMessages();
		HashMap<String, String> txtMessageMap = new HashMap<>();
		txtMessageMap =	receiveMessages();
		return txtMessageMap;
	}	
	
	private HashMap<String,String> receiveMessages() {
		String text = "";
		HashMap<String, String> txtMessageMap = new HashMap<>();
		try {
			Context initCtx = new InitialContext();
			QueueConnectionFactory connectionFactory = (QueueConnectionFactory) initCtx
					.lookup("java:comp/env/jms/ConnectionFactory");
			QueueConnection queueConnection = connectionFactory.createQueueConnection();
			QueueSession queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue queue = (Queue) initCtx.lookup("java:comp/env/jms/queue/MyQueue");
			QueueReceiver receiver = queueSession.createReceiver(queue);

			queueConnection.start();
			try {
				while(true){
				Message m = receiver.receive(1000);
				
				if (m != null && m instanceof TextMessage) {
					TextMessage tm = (TextMessage) m;
					//text = text+","+tm.getText();
					txtMessageMap.put(tm.getJMSCorrelationID(), tm.getText());
					System.out.println("Received TextMessage with text '%s'."+text);
				} else {
					System.out.println("No TextMessage received: '%s'"+text);
					break;
				}
				}
			} finally {
				queueSession.close();
				queueConnection.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return txtMessageMap;
	}
	
	
}
